#!/bin/bash - 
#===============================================================================
#
#          FILE: setting.sh
# 
#         USAGE: ./setting.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: YOUR NAME (), 
#  ORGANIZATION: 
#       CREATED: 07/25/2019 13:16
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error
server1 0.0.0.0
server2 1.1.1.1

